# Returns the result of multiplying x and y.
def multiply_numbers(x, y):
    return x * y


def test_multiply_numbers():
    assert multiply_numbers(2, 3) == 6
